#include <px4_defines.h>
#include <stdint.h>
#include <sys/ioctl.h>

/* Declaração da função gps_orb_subscribe(), responsável por sobreescrever um tópico,
 * possibilitando assim a recepção de dados entre um processo e outro.
 */
void gps_orb_subscribe(void);
/* Declaração da função gps_orb_copy(), responsável por copiar os valores da struct
 * de dados contida do tópico vehicle_gps_position.
 */
void gps_orb_copy(void);
/* Declaração da função gps_orb_publish(), responsável por publicar os valores atuais
 * no tópico vehicle_atitude.
 */
void gps_orb_publish(void);
/* Declaração da função gps_latitude(), responsável por retornar o valor atualizado
 * da latitude obtido por meio da função gps_orb_copy.
 */
double gps_latitude(void);
/* Declaração da função gps_longitude(), responsável por retornar o valor atualizado
 * da longitude obtido por meio da função gps_orb_copy.
 */
double gps_longitude(void);
/* Declaração da função gps_altitude(), responsável por retornar o valor atualizado
 * da altitude obtido por meio da função gps_orb_copy.
 */
double gps_altitude(void);
/* Declaração da função sensor_combined_orb_subscribe(), responsável por
 * sobreescrever um tópico, possibilitando assim a recepção de dados entre um
 * processo e outro.
 */
void sensor_combined_orb_subscribe(void);
/* Declaração da função sensor_combined_orb_set_interval, responsável por definir
 * a frequência de leitura dos sensores.
 */
void sensor_combined_orb_set_interval(int);
/* Declaração da função gps_orb_advertise(), responsável por...
 *
 */
void sensor_combined_orb_advertise(void);
/* Declaração da função sensor_combined_orb_copy(), responsável por copiar os
 * valores da struct de dados contida do tópico vehicle_gps_position.
 */
void sensor_combined_orb_copy(void);
/* Declaração da função vehicle_attitude_orb_publish(), responsável por publicar
 * os valores atualizados no tópico vehicle_attitude.
 */
void vehicle_attitude_orb_publish(void);
/* Declaração da função valor_0_acelerometro(), responsável por retornar
 * o primeiro valor gerado pelo acelerômetro.
 */
double valor_0_acelerometro(void);
/* Declaração da função valor_1_acelerometro(), responsável por retornar
 * o segundo valor gerado pelo acelerômetro.
 */
double valor_1_acelerometro(void);
/* Declaração da função valor_2_acelerometro(), responsável por retornar
 * o terceiro valor gerado pelo acelerômetro.
 */
double valor_2_acelerometro(void);
/* Declaração da função valor_0_magnetômetro(), responsável por retornar
 * o primeiro valor gerado pelo magnetômetro.
 */
double valor_0_magnetometro(void);
/* Declaração da função valor_1_magnetômetro(), responsável por retornar
 * o segundo valor gerado pelo magnetômetro.
 */
double valor_1_magnetometro(void);
/* Declaração da função valor_2_magnetômetro(), responsável por retornar
 * o terceiro valor gerado pelo magnetômetro.
 */
double valor_2_magnetometro(void);
/* Declaração da função valor_0_giroscópio(), responsável por retornar
 * o primeiro valor gerado pelo giroscópio.
 */
double valor_0_giroscopio(void);
/* Declaração da função valor_1_giroscópio(), responsável por retornar
 * o segundo valor gerado pelo giroscópio.
 */
double valor_1_giroscopio(void);
/* Declaração da função valor_2_giroscópio(), responsável por retornar
 * o terceiro valor gerado pelo giroscópio.
 */
double valor_2_giroscopio(void);
/*Declaração da função abrir_driver(), responsável por realizar a abertura do
 * driver drv_pwm_output
 */
void abrir_driver(void);
/*Função para armação dos ESCs.*/
void armar_esc(void);
/*Função para habilitação da chave de segurança.*/
void armar_chave_seguranca(void);
/*Função que aciona o servo 1 por meio do ioctl.*/
void ioctl_servo_um(int);
/*Função que aciona o servo 2 por meio do ioctl.*/
void ioctl_servo_dois(int);
/*Função que aciona o servo 3 por meio do ioctl.*/
void ioctl_servo_tres(int);
/*Função que aciona o servo 4 por meio do ioctl.*/
void ioctl_servo_quatro(int);
