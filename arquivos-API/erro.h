#include <px4_defines.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include "api.h"

#define DEBUG

int erro_chamada_orb_set_interval(void);
int erro_chamada_orb_advertise(void);
int erro_chamada_orb_copy(void);
int erro_chamada_orb_leitura(void);
int erro_descritor_arquivo(void);
int erro_armar_chave_seguranca(void);
int erro_armar_esc(void);
int erro_ioctl_esc(void);

int erro_chamada_orb_set_interval(){


	#ifdef DEBUG
			PX4_INFO("ERRO: VERIFIQUE SE A FUNÇÃO ORB_SUBSCRIBE FOI CHAMADA "
				 "ANTERIORMENTE ");
			return 1;

	#else
			return 0;
	#endif
}

int erro_chamada_orb_advertise(){

	#ifdef DEBUG
		PX4_INFO("ERRO: VERIFIQUE SE A FUNÇÃO ORB_SET_INTERVAL FOI CHAMADA "
			 "ANTERIORMENTE ");
		return 1;
	#else
		return 0;
	#endif
}

int erro_chamada_orb_copy(){
	#ifdef DEBUG
		PX4_INFO("ERRO: VERIFIQUE SE A FUNÇÃO ORB_ADVERTISE FOI CHAMADA "
			 "ANTERIORMENTE ");
	 	return 1;
	#else
		return 0;
	#endif
}

int erro_chamada_orb_leitura(){
	#ifdef DEBUG
		PX4_INFO("ERRO: VERIFIQUE SE A FUNÇÃO ORB_COPY FOI CHAMADA "
			 "ANTERIORMENTE, NÃO HÁ VALORES DISPONÍVEIS PARA SEREM LIDOS ");
	 	return 1;
	#else
		return 0;
	#endif
}


int erro_descritor_arquivo(){
	#ifdef DEBUG
		PX4_INFO("ERRO: O DRIVER NÃO PODE SER ABERTO CORRETAMENTE. TENTE EXECUTAR"
			 "O PROGRAMA NOVAMENTE");
		return 1;
	#else
		return 0;
	#endif
}

int erro_armar_chave_seguranca(){

	#ifdef DEBUG
		PX4_INFO("ERRO: A CHAVE DE SEGURANÇA NÃO PODE SER ARMADA. VERIFIQUE SE"
				 "O DRIVER FOI ABERTO");
		return 1;
	#else
		return 0;
	#endif
}

int erro_armar_esc(){

	#ifdef DEBUG
		PX4_INFO("ERRO: O ESC NÃO PODE SER ARMADO. VERIFIQUE SE A CHAVE DE SEGURANÇA"
				 "FOI ARMADA");
		return 1;
	#else
		return 0;
	#endif
}

int erro_ioctl_esc(){

	#ifdef DEBUG
		PX4_INFO("ERRO: O ESC NÃO PODE SER ACIONADO. VERIFIQUE SE O ESC FOI ARMADO"
				 "CORRETAMENTE");
		return 1;
	#else
		return 0;
	#endif
}
