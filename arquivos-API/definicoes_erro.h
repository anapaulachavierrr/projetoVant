#include "erro.h"
#include <px4_defines.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>


#define ERRO_CHAMADA_ORB_SET_INTERVAL erro_chamada_orb_set_interval();
#define ERRO_CHAMADA_ORB_ADVERTISE erro_chamada_orb_advertise();
#define ERRO_CHAMADA_ORB_COPY erro_chamada_orb_copy();
#define ERRO_CHAMADA_ORB_LEITURA erro_chamada_orb_leitura();
#define ERRO_DESCRITOR_ARQUIVO erro_descritor_arquivo();

#define ERRO_ARMAR_CHAVE_SEGURANCA erro_armar_chave_seguranca();
#define ERRO_ARMAR_ESC erro_armar_esc();
#define ERRO_IOCTL_ESC erro_ioctl_esc();

