#include <px4_config.h>
#include <px4_defines.h>
#include <px4_posix.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <poll.h>
#include <fcntl.h>
#include <float.h>
#include <errno.h>
#include <limits.h>
#include <math.h>

#include <uORB/uORB.h>
#include <uORB/topics/vehicle_gps_position.h>
#include <uORB/topics/sensor_combined.h>
#include <uORB/topics/vehicle_attitude.h>
#include <drivers/drv_pwm_output.h>
#include <uORB/topics/vehicle_attitude.h>

#include "api.h"
#include "definicoes_erro.h"

int sensor_sub_fd  = 0;
orb_advert_t att_pub;
struct vehicle_attitude_s att;
struct sensor_combined_s raw;

double valor_0_acel= 0.0;
double valor_1_acel = 0.0;
double valor_2_acel = 0.0;

double valor_0_mag= 0.0;
double valor_1_mag = 0.0;
double valor_2_mag = 0.0;

double valor_0_giro= 0.0;
double valor_1_giro = 0.0;
double valor_2_giro = 0.0;

/*Variável de teste */
int status=-1;
//void gps_orb_subscribe(void);

void sensor_combined_orb_subscribe(){
		status=0;
		sensor_sub_fd = orb_subscribe(ORB_ID(sensor_combined));
}

void sensor_combined_orb_set_interval(int freq){
	if(status==0){
		orb_set_interval(sensor_sub_fd, freq);
		status=1;
	}else
		ERRO_CHAMADA_ORB_SET_INTERVAL;
}

void sensor_combined_orb_advertise(){
	if(status==1){
		memset(&att, 0, sizeof(att));
		att_pub = orb_advertise(ORB_ID(vehicle_attitude), &att);
		status=2;
	}else
		ERRO_CHAMADA_ORB_ADVERTISE;
}

void sensor_combined_orb_copy(){
	if(status==2){
		orb_copy(ORB_ID(sensor_combined), sensor_sub_fd, &raw);
		status=3;
	}else
		ERRO_CHAMADA_ORB_COPY;
}

void vehicle_attitude_orb_publish(){
		orb_publish(ORB_ID(vehicle_attitude), att_pub, &att);
}

double valor_0_acelerometro(){
	if(status==3){
		valor_0_acel = raw.accelerometer_m_s2[0];
		return valor_0_acel;
	}
	else{
		ERRO_CHAMADA_ORB_LEITURA;
		return 0;
	}
}

double valor_1_acelerometro(){
	if(status==3){
		valor_1_acel = raw.accelerometer_m_s2[1];
		return valor_1_acel;
	}else{
		ERRO_CHAMADA_ORB_LEITURA;
		return 0;
	}
}

double valor_2_acelerometro(){
	if(status==3){
		valor_2_acel = raw.accelerometer_m_s2[2];
		return valor_2_acel;
	}else{
		ERRO_CHAMADA_ORB_LEITURA;
		return 0;
	}
}

double valor_0_magnetometro(){
	if(status==3){
		valor_0_mag = raw.magnetometer_ga[0];
		return valor_0_mag;
	}else{
		ERRO_CHAMADA_ORB_LEITURA;
		return 0;
	}
}

double valor_1_magnetometro(){
	if(status==3){
		valor_1_mag = raw.magnetometer_ga[1];
		return valor_1_mag;
	}else{
		ERRO_CHAMADA_ORB_LEITURA;
		return 0;
	}
}

double valor_2_magnetometro(){
	if(status==3){
		valor_2_mag = raw.magnetometer_ga[2];
		return valor_2_mag;
	}else{
		ERRO_CHAMADA_ORB_LEITURA;
		return 0;
	}
}

double valor_0_giroscopio(){
	if(status==3){
		valor_0_giro = raw.gyro_rad[0];
		return valor_0_giro;
	}else{
		ERRO_CHAMADA_ORB_LEITURA;
		return 0;
	}
}

double valor_1_giroscopio(){
	if(status==3){
		valor_1_giro = raw.gyro_rad[1];
		return valor_1_giro;
	}else{
		ERRO_CHAMADA_ORB_LEITURA;
		return 0;
	}
}

double valor_2_giroscopio(){
	if(status==3){
		valor_2_giro = raw.gyro_rad[2];
		return valor_2_giro;
	}else{
		ERRO_CHAMADA_ORB_LEITURA;
		return 0;
	}
}

struct vehicle_gps_position_s gps;
int sub_gps;
orb_advert_t gps_pub;

double latitude;
double longitude;
double altitude;

//void gps_orb_subscribe(void);

void gps_orb_subscribe(){
	status = 0;
	sub_gps = orb_subscribe(ORB_ID(vehicle_gps_position));

}

void gps_orb_copy(){

	if(status == 0){
		orb_copy(ORB_ID(vehicle_gps_position), sub_gps, &gps);
		status =3;
	}else{
		ERRO_CHAMADA_ORB_COPY;
	}
}

void gps_orb_publish(){
		orb_publish(ORB_ID(vehicle_gps_position), gps_pub, &gps);
}

double gps_latitude(){

	//if(status == 1){
		double lat = gps.lat;
		double lataux = lat;
		latitude = (lataux * 1e-7);
		return latitude;
	//}else{
	//	ERRO_CHAMADA_ORB_LEITURA;
	//	return 0;
	//}
}

double gps_longitude(){

//	if(status == 1){
		longitude = gps.lon/ 1.0e7;
		return longitude;
	//}else{
	//	ERRO_CHAMADA_ORB_LEITURA;
	//	return 0;
	//}
}

double gps_altitude(){

	//if(status==1){
		altitude = gps.alt/ 1e3f;
		return altitude;
	//}else{
		//ERRO_CHAMADA_ORB_LEITURA;
		//return 0;
	//}
}

char *dev_pwm = PWM_OUTPUT_BASE_DEVICE_PATH;
int pwm;

void abrir_driver(void){

    pwm = open(dev_pwm, 0);

    if(pwm<0){
	    ERRO_DESCRITOR_ARQUIVO
    }
	else{
	    status = 0;
	}
}
void armar_chave_seguranca(void){

	if(status == 0){
		ioctl(pwm, PWM_SERVO_SET_ARM_OK, 0);
		status = 1;
	}else{
		ERRO_ARMAR_CHAVE_SEGURANCA;
	}
}

void armar_esc(void){

	if(status == 1){
		ioctl(pwm, PWM_SERVO_ARM, 0);
		status = 2;
	}else{
		ERRO_ARMAR_ESC;
	}
}

void ioctl_servo_um(int periodo){

	if(status == 2){
		ioctl(pwm, PWM_SERVO_SET(0), periodo);
	}else{
		ERRO_IOCTL_ESC;
	}
}
void ioctl_servo_dois(int periodo){

	if(status == 2){
		ioctl(pwm, PWM_SERVO_SET(1), periodo);
	}else{
		ERRO_ARMAR_ESC;
	}
}
void ioctl_servo_tres(int periodo){
	if(status == 2){
		ioctl(pwm, PWM_SERVO_SET(2), periodo);
	}else{
		ERRO_ARMAR_ESC;
	}
}
void ioctl_servo_quatro(int periodo){
	if(status == 2){
		ioctl(pwm, PWM_SERVO_SET(3), periodo);
	}else{
		ERRO_ARMAR_ESC;
	}
}
