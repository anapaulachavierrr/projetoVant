/*
 * definicoes.h
 *
 *  Created on: 23/02/2017
 *      Author: ana
 */
#include "api.h"
#include <px4_defines.h>
#include <stdint.h>
#include <sys/ioctl.h>

/**
 * @mainpage
 * \section  API
 ** \subsection Objetivo
 ** \par
 * Esta documenta&ccedil;&atilde;o apresenta fun&ccedil;&otilde;es para leitura de
 * sensores e acionamento de atuadores de VANTs por meio da placa de controle
 * Pixhawk. Estas fun&ccedil;&otilde;es s&atilde;o baseadas principalmente na estrutura
 * de t&oacute;picos presente na Pixhawk, entretanto, para o acionamento de atuadores foi
 * implementada tamb&eacute;m uma fun&ccedil;&atilde;o que utiliza a manipula&ccedil;&atilde;o de device drivers.
 *
 *
 * \image html px4.png
 *  <CENTER> Figura 1- Placa de controle Pixhawk. </CENTER>
 * \subsection Contexto
 *
 *  \par
 *   Pixhawk &eacute; um sistema de piloto autom&aacute;tico avan&ccedil;ado projetado pelo projeto
 *  PX4-hardware aberto e fabricado pela Robotics 3D. Possui processador avan&ccedil;ado
 *  e tecnologia de sensor de ST Microelectronics&reg; e um sistema operacional NuttX
 *  em tempo real, que proporciona um desempenho incr&iacute;vel, flexibilidade e
 *  confiabilidade para controlar qualquer ve&iacute;culo aut&ocirc;nomo.
 *  Esta placa possui um conjunto de sensores internos (aceler&ocirc;metro, girosc&oacute;pio
 *  e magnet&ocirc;metro), al&eacute;m de um sensor de GPS, que deve ser conectado externamente.
 *  Existe uma camada b&aacute;sica de arquivos, que realiza a interface em baixo n&iacute;vel
 *  entre os perif&eacute;ricos (sensores e atuadores) do sistema e o software que
 *  descreve o mesmo. Estes arquivos s&atilde;o chamados de drivers, os quais realizam a
 *  leitura de sensores e acionamento de atuadores (servo motores ou h&eacute;lices).
 *  A Pixhawk oferece suporte para os tipos de comunica&ccedil;&atilde;o CAN, I2C, UART e SPI,
 *  al&eacute;m de sa&iacute;das para sinal do tipo PWM (Pulse Width Modulation),
 *  importante para o acionamento das h&eacute;lices de um VANT.
 *
 * \n
 *  \image html estrutura.png
 *  <CENTER> Figura 2- Estrutura modular do sistema da Pixhawk. </CENTER>
 *
 * \n
 * \par
 *  A camada uORB, que pode ser vista claramente no diagrama modular apresentado
 *  na Figura 2, possibilita uma comunica&ccedil;&atilde;o eficaz no ambiente multithreads da
 *  Pixhawk, pois permite uma atualiza&ccedil;&atilde;o constante, em curto intervalo de tempo,
 *  dos valores de sensores e status dos atuadores conectados ao sistema, al&eacute;m de
 *  apresentar importantes dados para as aplica&ccedil;&otilde;es de telemetria. Para tanto, s&atilde;o
 *  utilizados t&oacute;picos - arquivos que possuem informa&ccedil;&otilde;es relacionadas com uma ou
 *  mais informa&ccedil;&otilde;es espec&iacute;ficas, como, por exemplo, valores de um ou mais
 *  sensores. Quando uma aplica&ccedil;&atilde;o deseja acessar o valor do sensor aceler&ocirc;metro,
 *  por exemplo, realiza a abertura do t&oacute;pico correspondente a este sensor a uma
 *  frequ&ecirc;ncia previamente definida. Para a obten&ccedil;&atilde;o do valor de um sensor,
 *  uma aplica&ccedil;&atilde;o realiza a inscri&ccedil;&atilde;o em um determinado t&oacute;pico e define a taxa
 *  com que deseja acessar ao mesmo. Da mesma forma, para atualizar um determinado
 *  valor ou o status de um atuador , a aplica&ccedil;&atilde;o acessa o t&oacute;pico correspondente
 *  ao mesmo. Esta camada permite, portanto, o acesso &agrave;  informa&ccedil;&otilde;es sem que seja
 *  necess&aacute;rio o acesso direto aos drivers de dispositivo pelas aplica&ccedil;&otilde;es.
 *
 *\subsection sub Estrutura&ccedil;&atilde;o
 *\par
 *Esta API est&aacute; organizada em:
 *\n
 *\n
 *\li \ref page1
 *\li \ref Ficheiros
 *\n
 *\n
 *Em "ficheiros" tem-se a documenta&ccedil;&atilde;o de todas as fun&ccedil;&otilde;es
 *e MACROS implementados. Em "exemplos" h&aacute; alguns c&oacute;digos simples que exemplificam
 *a utiliza&ccedil;&atilde;o da API.
 *API
 *@author Ana Paula C. Rodrigues
 */


/*! \page page1 Exemplos
  \tableofcontents
  Esta p&aacute;gina cont&eacute;m as se&ccedil;&otilde;es \ref sec1 and \ref sec2.
  \section sec1 Acionamento de atuadores por meio de driver
	O diagrama abaixo mostra os passos seguidos para o acionamento dos quatro
	atuadores de um quadrirotor realizado pelo c&oacute;digo a seguir. Neste
	exemplo, o per&iacute;odo do PWM enviado aos ESCs dos respectivos motores foi
	de 1500.
  \n
  \dot
digraph example_api_graph {
	node [shape=box];
	ABRIR_DRIVER  [fillcolor=lightblue,style=filled,label="ABRIR_DRIVER",  URL="\ref ABRIR_DRIVER" ];
	ARMAR_ESC [fillcolor=lightblue,style=filled,label="ARMAR_ESC",  URL="\ref ARMAR_ESC" ];
	ARMAR_CHAVE_SEGURANCA [fillcolor=lightblue,style=filled,label="ARMAR_CHAVE_SEGURANCA",  URL="\ref ARMAR_CHAVE_SEGURANCA" ];
	IOCTL_SERVO_UM [fillcolor=lightblue,style=filled,label="IOCTL_SERVO_UM",  URL="\ref IOCTL_SERVO_UM(periodo)" ];
	IOCTL_SERVO_DOIS [fillcolor=lightblue,style=filled,label="IOCTL_SERVO_DOIS",  URL="\ref IOCTL_SERVO_DOIS(periodo)" ];
	IOCTL_SERVO_TRES [fillcolor=lightblue,style=filled,label="IOCTL_SERVO_TRES",  URL="\ref IOCTL_SERVO_TRES(periodo)" ];
	IOCTL_SERVO_QUATRO [fillcolor=lightblue,style=filled,label="IOCTL_SERVO_QUATRO",  URL="\ref IOCTL_SERVO_QUATRO(periodo)" ];
	ABRIR_DRIVER -> ARMAR_ESC;
	ARMAR_ESC -> ARMAR_CHAVE_SEGURANCA;
	ARMAR_CHAVE_SEGURANCA -> IOCTL_SERVO_UM;
	ARMAR_CHAVE_SEGURANCA -> IOCTL_SERVO_DOIS;
	ARMAR_CHAVE_SEGURANCA -> IOCTL_SERVO_TRES;
	ARMAR_CHAVE_SEGURANCA -> IOCTL_SERVO_QUATRO;
}
\enddot
\n

\li \b ABRIR_DRIVER\b: representa a fun&ccedil;&atilde;o respons&aacute;vel por
"abrir" o c&oacute;digo do \a driver que realiza o acionamento dos atuadores por
meio de sinal PWM.
\n
\li \b ARMAR_ESC\b: MACRO da fun&ccedil;&atilde;o que realiza a
arma&ccedil;&atilde;o dos ESCs dos respectivos motores.
\n
\li \b ARMAR_CHAVE_SEGURANCA\b: representa a fun&ccedil;&atilde;o
respons&aacute;vel por armar as chaves de seguran&ccedil;a do sistema. Caso esta
fun&ccedil;&atilde;o seja chamada, os motores somente ser&aatilde;o acionados
quando a chave for pressionada.
\n
\li \b IOCLT_SERVO_UM(per&iacute;odo)\b: MACRO da fun&ccedil;&atilde;o que realiza
o acionamento dos motores. Deve ser passado como par&acirc;metro o per&iacute;odo
do sinal PWM desejado. Este per&iacute;odo pode variar entre 1000 e 2000 &mu;s.
&Eacute; semelhante &agrave;s MACROS \b IOCLT_SERVO_DOIS(per&iacute;odo)
IOCLT_SERVO_TRES(per&iacute;odo) e \b IOCLT_SERVO_QUATRO(per&iacute;odo),
respons&aacute;veis pelo acionamento dos motores dois, tr&ecirc;s e quatro,
respectivamente.
\n

Caso o c&oacute;digo implementado pelo usu&aacute;rio retorne algum tipo de erro,
o programa pode ser "debugado" por meio de algumas fun&ccedil;&otilde;es
implementadas. Para utilizar as fun&ccedil;&otilde;es para debug de c&oacute;digo,
basta acrescentar #define DEBUG no in&iacute;cio do c&oacute;digo
\ref erro.h

\code

#include <px4_posix.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <poll.h>
#include <fcntl.h>
#include <float.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>
#include <uORB/uORB.h>
#include <uORB/topics/vehicle_gps_position.h>
#include <uORB/topics/sensor_combined.h>
#include <uORB/topics/vehicle_attitude.h>
#include "drivers/api/definicoes.h"
#include "drivers/api/definicoes_erro.h"

__EXPORT int px4_simple_app_main(int argc, char *argv[]);

int px4_simple_app_main(int argc, char *argv[])
{

	ABRIR_DRIVER;
	ARMAR_ESC;
	ARMAR_CHAVE_SEGURANCA;
	IOCTL_SERVO_UM(1500);
	IOCTL_SERVO_DOIS(1500);
	IOCTL_SERVO_TRES(1500);
	IOCTL_SERVO_QUATRO(1500);
    return 0;
}
\endcode

  \section sec2 Utiliza&ccedil;&atilde;o de t&oacute;picos para leitura de sensores
	\n
	O diagrama abaixo mostra os passos seguidos para a leitura de sensores de um
	VANT. No c&oacute;digo a seguir s&atilde;o lidos os sensores aceler&ocirc;metro,
	girosc&oacute;pio, magnet&ocirc;metro e GPS. Os passos mostrados no diagrama
	s&atilde;o para a leitura do sensor girosc&oacute;pio.
	 \n
	 \n
  \dot
digraph example_api_graph {
	node [shape=box];
	ASSINATURA_SENSOR_COMBINED  [fillcolor=lightgray,style=filled,label="ASSINATURA_SENSOR_COMBINED",  URL="\ref ASSINATURA_SENSOR_COMBINED" ];
	INTERVALO_LEITURA_SENSOR_COMBINED [fillcolor=lightgray,style=filled,label="INTERVALO_LEITURA_SENSOR_COMBINED",  URL="\ref INTERVALO_LEITURA_SENSOR_COMBINED(freq)" ];
	AVISO_SENSOR_COMBINED [fillcolor=lightgray,style=filled,label="AVISO_SENSOR_COMBINED",  URL="\ref AVISO_SENSOR_COMBINED" ];
	COPIA_SENSOR_COMBINED [fillcolor=lightgray,style=filled,label="COPIA_SENSOR_COMBINED",  URL="\ref COPIA_SENSOR_COMBINED" ];
	VALOR_X_ACELEROMETRO [fillcolor=gray,style=filled,label="VALOR_X_ACELEROMETRO",  URL="\ref VALOR_X_ACELEROMETRO" ];
	VALOR_Y_ACELEROMETRO [fillcolor=gray,style=filled,label="VALOR_Y_ACELEROMETRO",  URL="\ref VALOR_Y_ACELEROMETRO" ];
	VALOR_Z_ACELEROMETRO [fillcolor=gray,style=filled,label="VALOR_Z_ACELEROMETRO",  URL="\ref VALOR_Z_ACELEROMETRO" ];
	ASSINATURA_SENSOR_COMBINED -> INTERVALO_LEITURA_SENSOR_COMBINED;
	INTERVALO_LEITURA_SENSOR_COMBINED  -> AVISO_SENSOR_COMBINED;
	AVISO_SENSOR_COMBINED -> COPIA_SENSOR_COMBINED;
	COPIA_SENSOR_COMBINED -> VALOR_X_ACELEROMETRO;
	COPIA_SENSOR_COMBINED -> VALOR_Y_ACELEROMETRO;
	COPIA_SENSOR_COMBINED -> VALOR_Z_ACELEROMETRO;
}
\enddot
\n

\li \b ASSINATURA_SENSOR_COMBINED\b: representa a fun&ccedil;&atilde;o respons&aacute;vel por
realizar a inscri&ccedil;&atilde;o para acesso ao t&oacute;pico \a sensor_combined.
Este t&oacute;pico realiza a leitura dos sensores aceler&ocirc;metro,
girosc&oacute;pio e magnet&ocirc;metro.  \n
\li \b INTERVALO_LEITURA_SENSOR_COMBINED\b: MACRO da fun&ccedil;&atilde;o que
passa como par&acirc;metro ao t&oacute;pico correspondente o per&iacute;odo com
que o usu&aacute;rio deseja acesso-lo. \n
\li \b AVISO_SENSOR_COMBINED\b: representa a fun&ccedil;&atilde;o
respons&aacute;vel por gerar um "aviso" de que o t&oacute;pico correspondente
ser&aacute; acessado pelo programa.
\n
\li \b COPIA_SENSOR_COMBINED\b: MACRO da fun&ccedil;&atilde;o que realiza a
c&oacute;pia dos valores dos sensores lidos pelo t&oacute;pico.
\n
\li \b VALOR_X_ACELEROMETRO\b: Fun&ccedil;&atilde;o que retorna um valor double
correspondente ao valor de acelera&ccedil;&atilde;o no eixo x. Semelhante &agrave;s
Fun&ccedil;&otilde;es \b VALOR_Y_ACELEROMETRO e \b VALOR_Z_ACELEROMETRO, que
retornam, respectivamente, os valores de acelera&ccedil;&atilde;o nos eicos Y e Z.
\n
\n
A seguir encontra-se o c&oacute;digo que realiza a leitura e impress&atilde;o para
o usu&aacute;rio dos valores dos sensores  aceler&ocirc;metro, girosc&oacute;pio,
magnet&ocirc;metro e GPS. \n
\code
#include <px4_config.h>
#include <px4_defines.h>
#include <px4_posix.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <poll.h>
#include <fcntl.h>
#include <float.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>
#include <uORB/uORB.h>
#include <uORB/topics/vehicle_gps_position.h>
#include <uORB/topics/sensor_combined.h>
#include <uORB/topics/vehicle_attitude.h>
#include "drivers/api/definicoes.h"
#include "drivers/api/definicoes_erro.h"

__EXPORT int px4_simple_app_main(int argc, char *argv[]);

int px4_simple_app_main(int argc, char *argv[])
{


	SENSOR_COMBINED_ORB_SUBSCRIBE;
	SENSOR_COMBINED_ORB_SET_INTERVAL(200);
	SENSOR_COMBINED_ORB_ADVERTISE;
	SENSOR_COMBINED_ORB_COPY;


	double valor_x_acel = VALOR_X_ACELEROMETRO;
	double valor_y_acel =  VALOR_Y_ACELEROMETRO;
	double valor_z_acel = VALOR_Z_ACELEROMETRO;

	double valor_x_giro = VALOR_X_GIROSCOPIO;
	double valor_y_giro =  VALOR_Y_GIROSCOPIO;
	double valor_z_giro = VALOR_Z_GIROSCOPIO;

	double valor_x_mag = VALOR_X_MAGNETOMETRO;
	double valor_y_mag =  VALOR_Y_MAGNETOMETRO;
	double valor_z_mag = VALOR_Z_MAGNETOMETRO;

	PX4_INFO("Aceleracao no eixo x: %.8f",
							 valor_x_acel);
	PX4_INFO("Aceleracao no eixo y: %.8f" ,
							 valor_y_acel);
	PX4_INFO("Aceleracao no eixo z:\t %.8f" ,
							 valor_z_acel);

	ASSINATURA_GPS
	COPIA_GPS

	double latitude = GPS_LATITUDE
	double longitude = GPS_LONGITUDE
	double altitude = GPS_ALTITUDE

	PX4_INFO("%.8f",
		                    latitude);
	PX4_INFO("Longitude: %.8f" ,
		   	   	   	   	   longitude);
	PX4_INFO("Altitude:\t %.8f" ,
		   	   	   	   	    altitude);

    return 0;
}



\endcode
*/

/** \Assinatura para leitura dos valores do sensor GPS */
#define ASSINATURA_GPS gps_orb_subscribe();
/** fun&ccedil;&atilde;o que copia os valores do sensor GPS */
#define COPIA_GPS gps_orb_copy();
/** fun&ccedil;&atilde;o para publicafun&ccedil;&atilde;o dos valores do sensor GPS */
#define PUBLICA_GPS gps_orb_publish();
/** fun&ccedil;&atilde;o para leitura do valor de latitude fornecido pelo sensor GPS
 * @return latitude */
#define GPS_LATITUDE gps_latitude();
/** fun&ccedil;&atilde;o para leitura do valor de longitude fornecido pelo sensor GPS
 * @return longitude*/
#define GPS_LONGITUDE gps_longitude();
/** @return fun&ccedil;&atilde;o para leitura do valor de altitude fornecido pelo sensor GPS
 * 	@return altitude */
#define GPS_ALTITUDE gps_altitude();

/** \Assinatura para leitura dos valores dos sensores aceler&ocirc;metro, girosc&oacute;pio e magnet&ocirc;metro */
#define ASSINATURA_SENSOR_COMBINED sensor_combined_orb_subscribe();
/** fun&ccedil;&atilde;o que define a frequ&ecirc;ncia de leitura dos valores dos sensores aceler&ocirc;metro, girosc&oacute;pio e magnet&ocirc;metro */
#define INTERVALO_LEITURA_SENSOR_COMBINED(freq) sensor_combined_orb_set_interval(freq);
/** fun&ccedil;&atilde;o para aviso de acesso ao t&oacute;pico que fornece os valores dos sensores aceler&ocirc;metro, girosc&oacute;pio e magnet&ocirc;metro */
#define AVISO_SENSOR_COMBINED sensor_combined_orb_advertise();
/** fun&ccedil;&atilde;o que copia os valores dos sensores aceler&ocirc;metro, girosc&oacute;pio e magnet&ocirc;metro */
#define COPIA_SENSOR_COMBINED sensor_combined_orb_copy();
/** fun&ccedil;&atilde;o para publicafun&ccedil;&atilde;o dos valores dos sensores aceler&ocirc;metro, girosc&oacute;pio e magnet&ocirc;metro */
#define PUBLICA_SENSOR_COMBINED sensor_combined_orb_publish();

/** fun&ccedil;&atilde;o para leitura do valor de acelera&ccedil;&atilde;o no eixo x fornecido pelo sensor aceler&ocirc;metro
 *  @return acelera&ccedil;&atilde;o no eixo x */
#define VALOR_X_ACELEROMETRO valor_0_acelerometro();

/** fun&ccedil;&atilde;o para leitura do valor de  acelera&ccedil;&atilde;o no eixo y fornecido pelo sensor aceler&ocirc;metro
 *  @return acelera&ccedil;&atilde;o no eixo y */
#define VALOR_Y_ACELEROMETRO valor_1_acelerometro();

/** fun&ccedil;&atilde;o para leitura do valor de acelera&ccedil;&atilde;o no eixo z fornecido pelo sensor aceler&ocirc;metro
 *  @return acelera&ccedil;&atilde;o no eixo z */
#define VALOR_Z_ACELEROMETRO valor_2_acelerometro();

/** fun&ccedil;&atilde;o para leitura do valor do eixo x fornecido pelo sensor girosc&oacute;pio
 *  @return no eixo x */
#define VALOR_X_GIROSCOPIO valor_0_giroscopio();

/** fun&ccedil;&atilde;o para leitura do valor do eixo y fornecido pelo sensor girosc&oacute;pio
 *  @return no eixo y */
#define VALOR_Y_GIROSCOPIO valor_1_giroscopio();

/** fun&ccedil;&atilde;o para leitura do valor do eixo z fornecido pelo sensor girosc&oacute;pio
 *  @return no eixo z*/
#define VALOR_Z_GIROSCOPIO valor_2_giroscopio();

/** fun&ccedil;&atilde;o para leitura do valor do eixo x fornecido pelo sensor magnet&ocirc;metro
 *  @return no eixo x */
#define VALOR_X_MAGNETOMETRO valor_0_magnetometro();

/** fun&ccedil;&atilde;o para leitura do valor do eixo y fornecido pelo sensor magnet&ocirc;metro
 *  @return no eixo y */
#define VALOR_Y_MAGNETOMETRO valor_1_magnetometro();

/** fun&ccedil;&atilde;o para leitura do valor do eixo z fornecido pelo sensor magnet&ocirc;metro
 *  @return no eixo z */
#define VALOR_Z_MAGNETOMETRO valor_2_magnetometro();

/** fun&ccedil;&atilde;o para abertura do driver para acionamento dos atuadores*/
#define ABRIR_DRIVER abrir_driver();

/** fun&ccedil;&atilde;o para arma&ccedil;&atilde;o dos ESC para acionamento dos atuadores */
#define ARMAR_ESC armar_esc();

/** fun&ccedil;&atilde;o para arma&ccedil;&atilde;o da chave de seguran&ccedil;a */
#define ARMAR_CHAVE_SEGURANCA armar_chave_seguranca();

/** fun&ccedil;&atilde;o para acionamento do atuador um do VANT. Deve-se passar como par&acir;cmetro o per&iacute;odo do sinal PWM  */
#define IOCTL_SERVO_UM(periodo) ioctl_servo_um(periodo);

/** fun&ccedil;&atilde;o para acionamento do atuador dois do VANT. Deve-se passar como par&acirc;metro o per&iacute;odo do sinal PWM  */
#define IOCTL_SERVO_DOIS(periodo) ioctl_servo_dois(periodo);

/** fun&ccedil;&atilde;o para acionamento do atuador tr&ecirc;s do VANT. Deve-se passar como par&acirc;metro o per&iacute;odo do sinal PWM  */
#define IOCTL_SERVO_TRES(periodo) ioctl_servo_tres(periodo);

/** fun&ccedil;&atilde;o para acionamento do atuador quatro do VANT. Deve-se passar como par&acirc;metro o per&iacute;odo do sinal PWM  */
#define IOCTL_SERVO_QUATRO(periodo) ioctl_servo_quatro(periodo);


