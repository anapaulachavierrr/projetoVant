#include <stdio.h>
#include <fcntl.h>
#include <assert.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int ret=0;
	
	assert(argc>1);
	char buff[100];
	char i=0;
	
	memset(buff,0,100);
	printf("Input:%s\n",argv[1]);
	
	int fd=open("/dev/chardev",O_RDWR);
	if(fd<0)
	{
		printf("Error on open the device!\n");
	}
	
	printf("Device openned sucessfully!\n");
	ret=write(fd,argv[1],strlen(argv[1]),0);
	if(ret<0)
	{
	    fprintf(stderr, "write: %s\n", strerror(-ret));
		return -1;		
	}
	
	while(read(fd,&buff[i++],1));
	
	printf("Reserved by the driver: %s\n",buff);
	
	return 0;


}
