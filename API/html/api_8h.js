var api_8h =
[
    [ "abrir_driver", "api_8h.html#a51fc4c12d93f91a8da4ec842b014b6a6", null ],
    [ "armar_chave_seguranca", "api_8h.html#a8c7aa54d67a46070ed0374e053270091", null ],
    [ "armar_esc", "api_8h.html#a81b19e1095b10d761f2d8a0f5b6d7e63", null ],
    [ "gps_altitude", "api_8h.html#a45522dbc77631bc229900bd248342726", null ],
    [ "gps_latitude", "api_8h.html#a3ffec7eff5d7c969d621a60c117bbabc", null ],
    [ "gps_longitude", "api_8h.html#a2ba2623d928dc71ee98ef31fde85720a", null ],
    [ "gps_orb_copy", "api_8h.html#a7231b5c1f17597c4d704a629af4b80ea", null ],
    [ "gps_orb_publish", "api_8h.html#ab7363627d10360166d1a96fc2623f645", null ],
    [ "gps_orb_subscribe", "api_8h.html#aaa00efff714f27b5be829f07d6ed3d7a", null ],
    [ "ioctl_servo_dois", "api_8h.html#ab7a1d0d6487dfb28699f00e2e22ac411", null ],
    [ "ioctl_servo_quatro", "api_8h.html#ae50838a30f5072ba67a469c54431b63f", null ],
    [ "ioctl_servo_tres", "api_8h.html#a7a82246dc2f774934b2226a1bf46c7eb", null ],
    [ "ioctl_servo_um", "api_8h.html#acb41425c3d2c129612377fa5ecfd7825", null ],
    [ "sensor_combined_orb_advertise", "api_8h.html#a83d24a77b5e2e164614baf7909ee37af", null ],
    [ "sensor_combined_orb_copy", "api_8h.html#a9c4f268518bbb8ec0b3994cd58d5f82c", null ],
    [ "sensor_combined_orb_set_interval", "api_8h.html#a6aebabefa933ff7eccf9da862f355f1a", null ],
    [ "sensor_combined_orb_subscribe", "api_8h.html#ac39e23677ca72a58e7e62dd082f5836f", null ],
    [ "valor_0_acelerometro", "api_8h.html#a359b1cc6eaf622e605efa94203985432", null ],
    [ "valor_0_giroscopio", "api_8h.html#a71aeb0a110e073de09abdf258655b848", null ],
    [ "valor_0_magnetometro", "api_8h.html#ad3bb8042759cd22f0ff8a7caddbd8329", null ],
    [ "valor_1_acelerometro", "api_8h.html#a58df994cbf66a710821344b4b4e6869c", null ],
    [ "valor_1_giroscopio", "api_8h.html#a3a139493556e5cf5513251b8f8f2395c", null ],
    [ "valor_1_magnetometro", "api_8h.html#a55e05e63a53f5f67685631abdea9068f", null ],
    [ "valor_2_acelerometro", "api_8h.html#a99180edf9f1db148175f07f924916e19", null ],
    [ "valor_2_giroscopio", "api_8h.html#ab452f2fba715ede6e65a1e563f63f022", null ],
    [ "valor_2_magnetometro", "api_8h.html#ac026a82661824410d505236d9209cffc", null ],
    [ "vehicle_attitude_orb_publish", "api_8h.html#a8e5c982447b790bc16b961f899599099", null ]
];