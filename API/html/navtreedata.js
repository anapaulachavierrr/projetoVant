var NAVTREE =
[
  [ "Página principal", "index.html", [
    [ "API", "index.html#API", [
      [ "Objetivo", "index.html#Objetivo", null ],
      [ "Contexto", "index.html#Contexto", null ],
      [ "Estruturação", "index.html#sub", null ]
    ] ],
    [ "Exemplos", "page1.html", [
      [ "Acionamento de atuadores por meio de driver", "page1.html#sec1", null ],
      [ "Utilização de tópicos para leitura de sensores", "page1.html#sec2", null ]
    ] ],
    [ "Ficheiros", null, [
      [ "Lista de ficheiros", "files.html", "files" ],
      [ "Globais", "globals.html", [
        [ "Tudo", "globals.html", null ],
        [ "Funções", "globals_func.html", null ],
        [ "Variáveis", "globals_vars.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"api_8c.html"
];

var SYNCONMSG = 'clique para desativar a sincronização do painel';
var SYNCOFFMSG = 'clique para ativar a sincronização do painel';