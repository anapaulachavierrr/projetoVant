var searchData=
[
  ['ioctl_5fservo_5fdois',['ioctl_servo_dois',['../api_8c.html#a205b416b5985de06d07d26e9b1f69d1d',1,'ioctl_servo_dois(int periodo):&#160;api.c'],['../api_8h.html#ab7a1d0d6487dfb28699f00e2e22ac411',1,'ioctl_servo_dois(int):&#160;api.c']]],
  ['ioctl_5fservo_5fquatro',['ioctl_servo_quatro',['../api_8c.html#a5c736a238ed94806e99cf5bafaa4bfad',1,'ioctl_servo_quatro(int periodo):&#160;api.c'],['../api_8h.html#ae50838a30f5072ba67a469c54431b63f',1,'ioctl_servo_quatro(int):&#160;api.c']]],
  ['ioctl_5fservo_5ftres',['ioctl_servo_tres',['../api_8c.html#aabe4510e9574018efd2d839b79dffae2',1,'ioctl_servo_tres(int periodo):&#160;api.c'],['../api_8h.html#a7a82246dc2f774934b2226a1bf46c7eb',1,'ioctl_servo_tres(int):&#160;api.c']]],
  ['ioctl_5fservo_5fum',['ioctl_servo_um',['../api_8c.html#ad8b85483dc4d65790eca9e4fd7e1b3ff',1,'ioctl_servo_um(int periodo):&#160;api.c'],['../api_8h.html#acb41425c3d2c129612377fa5ecfd7825',1,'ioctl_servo_um(int):&#160;api.c']]]
];
