var searchData=
[
  ['abrir_5fdriver',['ABRIR_DRIVER',['../definicoes_8h.html#ad43c83a4144d173de3495f7ac04e9051',1,'ABRIR_DRIVER():&#160;definicoes.h'],['../api_8c.html#a51fc4c12d93f91a8da4ec842b014b6a6',1,'abrir_driver(void):&#160;api.c'],['../api_8h.html#a51fc4c12d93f91a8da4ec842b014b6a6',1,'abrir_driver(void):&#160;api.c']]],
  ['altitude',['altitude',['../api_8c.html#a2b13d276aee0d9fd646c8fa3647e869b',1,'api.c']]],
  ['api_2ec',['api.c',['../api_8c.html',1,'']]],
  ['api_2eh',['api.h',['../api_8h.html',1,'']]],
  ['armar_5fchave_5fseguranca',['ARMAR_CHAVE_SEGURANCA',['../definicoes_8h.html#afff3d9065397b83d7b0db5a6c06fab7b',1,'ARMAR_CHAVE_SEGURANCA():&#160;definicoes.h'],['../api_8c.html#a8c7aa54d67a46070ed0374e053270091',1,'armar_chave_seguranca(void):&#160;api.c'],['../api_8h.html#a8c7aa54d67a46070ed0374e053270091',1,'armar_chave_seguranca(void):&#160;api.c']]],
  ['armar_5fesc',['ARMAR_ESC',['../definicoes_8h.html#abdb5cae0e38c3b9d3bf35b497f8def5f',1,'ARMAR_ESC():&#160;definicoes.h'],['../api_8c.html#a81b19e1095b10d761f2d8a0f5b6d7e63',1,'armar_esc(void):&#160;api.c'],['../api_8h.html#a81b19e1095b10d761f2d8a0f5b6d7e63',1,'armar_esc(void):&#160;api.c']]],
  ['assinatura_5fgps',['ASSINATURA_GPS',['../definicoes_8h.html#a88c7dc73b167452cc7a88582c430c050',1,'definicoes.h']]],
  ['assinatura_5fsensor_5fcombined',['ASSINATURA_SENSOR_COMBINED',['../definicoes_8h.html#a170f9da243942e6d660eb333aa0d87b7',1,'definicoes.h']]],
  ['att',['att',['../api_8c.html#a956945cc98a127fb5c5a95740f23e8b4',1,'api.c']]],
  ['att_5fpub',['att_pub',['../api_8c.html#ae728b11b19faf114a6acc48d2e83be14',1,'api.c']]],
  ['aviso_5fsensor_5fcombined',['AVISO_SENSOR_COMBINED',['../definicoes_8h.html#a817d7a1bb005c15227dbaf45c6f0794b',1,'definicoes.h']]]
];
