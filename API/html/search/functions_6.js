var searchData=
[
  ['valor_5f0_5facelerometro',['valor_0_acelerometro',['../api_8c.html#a60c21fca92588bdc3ea59fabc92ce480',1,'valor_0_acelerometro():&#160;api.c'],['../api_8h.html#a359b1cc6eaf622e605efa94203985432',1,'valor_0_acelerometro(void):&#160;api.c']]],
  ['valor_5f0_5fgiroscopio',['valor_0_giroscopio',['../api_8c.html#a5177f3f33bb344108f6bdeef97ee4b51',1,'valor_0_giroscopio():&#160;api.c'],['../api_8h.html#a71aeb0a110e073de09abdf258655b848',1,'valor_0_giroscopio(void):&#160;api.c']]],
  ['valor_5f0_5fmagnetometro',['valor_0_magnetometro',['../api_8c.html#a0d114c207d9c1254601ad10fedb66408',1,'valor_0_magnetometro():&#160;api.c'],['../api_8h.html#ad3bb8042759cd22f0ff8a7caddbd8329',1,'valor_0_magnetometro(void):&#160;api.c']]],
  ['valor_5f1_5facelerometro',['valor_1_acelerometro',['../api_8c.html#aa14f31e667cf4353214cf0ae2c530e67',1,'valor_1_acelerometro():&#160;api.c'],['../api_8h.html#a58df994cbf66a710821344b4b4e6869c',1,'valor_1_acelerometro(void):&#160;api.c']]],
  ['valor_5f1_5fgiroscopio',['valor_1_giroscopio',['../api_8c.html#a3f166a8b433a2336e45bfafa382bbbaa',1,'valor_1_giroscopio():&#160;api.c'],['../api_8h.html#a3a139493556e5cf5513251b8f8f2395c',1,'valor_1_giroscopio(void):&#160;api.c']]],
  ['valor_5f1_5fmagnetometro',['valor_1_magnetometro',['../api_8c.html#a9d618652c951aa9044909c19bdfc163a',1,'valor_1_magnetometro():&#160;api.c'],['../api_8h.html#a55e05e63a53f5f67685631abdea9068f',1,'valor_1_magnetometro(void):&#160;api.c']]],
  ['valor_5f2_5facelerometro',['valor_2_acelerometro',['../api_8c.html#aa4284894604784dd0445e020c0d9ce1c',1,'valor_2_acelerometro():&#160;api.c'],['../api_8h.html#a99180edf9f1db148175f07f924916e19',1,'valor_2_acelerometro(void):&#160;api.c']]],
  ['valor_5f2_5fgiroscopio',['valor_2_giroscopio',['../api_8c.html#a89a890254f228d754d55f28b4d1324c5',1,'valor_2_giroscopio():&#160;api.c'],['../api_8h.html#ab452f2fba715ede6e65a1e563f63f022',1,'valor_2_giroscopio(void):&#160;api.c']]],
  ['valor_5f2_5fmagnetometro',['valor_2_magnetometro',['../api_8c.html#a03cbe386f0a1be3b9455273e1cc7e799',1,'valor_2_magnetometro():&#160;api.c'],['../api_8h.html#ac026a82661824410d505236d9209cffc',1,'valor_2_magnetometro(void):&#160;api.c']]],
  ['vehicle_5fattitude_5forb_5fpublish',['vehicle_attitude_orb_publish',['../api_8c.html#a9bad433c3c5bb8406d29e89e61e6411e',1,'vehicle_attitude_orb_publish():&#160;api.c'],['../api_8h.html#a8e5c982447b790bc16b961f899599099',1,'vehicle_attitude_orb_publish(void):&#160;api.c']]]
];
