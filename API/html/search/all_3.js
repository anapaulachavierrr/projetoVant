var searchData=
[
  ['erro_2eh',['erro.h',['../erro_8h.html',1,'']]],
  ['erro_5farmar_5fchave_5fseguranca',['ERRO_ARMAR_CHAVE_SEGURANCA',['../definicoes__erro_8h.html#a7585ae9495c7d1feb6878d47f2453ad4',1,'ERRO_ARMAR_CHAVE_SEGURANCA():&#160;definicoes_erro.h'],['../erro_8h.html#a44cca6ee8932257b062f08b147f420d5',1,'erro_armar_chave_seguranca(void):&#160;erro.h']]],
  ['erro_5farmar_5fesc',['ERRO_ARMAR_ESC',['../definicoes__erro_8h.html#a90e324b665f76b637a4f2ff06a578ebc',1,'ERRO_ARMAR_ESC():&#160;definicoes_erro.h'],['../erro_8h.html#a164c68dbe48b3a62d0c75f0be6a468b8',1,'erro_armar_esc(void):&#160;erro.h']]],
  ['erro_5fchamada_5forb_5fadvertise',['erro_chamada_orb_advertise',['../erro_8h.html#ab654577eee1e4d25896b022193dd799f',1,'erro_chamada_orb_advertise(void):&#160;erro.h'],['../definicoes__erro_8h.html#a41bac18d5034db98623365751cf1e068',1,'ERRO_CHAMADA_ORB_ADVERTISE():&#160;definicoes_erro.h']]],
  ['erro_5fchamada_5forb_5fcopy',['erro_chamada_orb_copy',['../erro_8h.html#ac45d986ede7a23a416b7ca3a3350be0f',1,'erro_chamada_orb_copy(void):&#160;erro.h'],['../definicoes__erro_8h.html#a41516322a553b24ac4181658e24deec3',1,'ERRO_CHAMADA_ORB_COPY():&#160;definicoes_erro.h']]],
  ['erro_5fchamada_5forb_5fleitura',['erro_chamada_orb_leitura',['../erro_8h.html#a6da9318ae53bba48df5e7a30118cfada',1,'erro_chamada_orb_leitura(void):&#160;erro.h'],['../definicoes__erro_8h.html#a048beb58ce2a791803f93e2a77c34a89',1,'ERRO_CHAMADA_ORB_LEITURA():&#160;definicoes_erro.h']]],
  ['erro_5fchamada_5forb_5fset_5finterval',['erro_chamada_orb_set_interval',['../erro_8h.html#a833106967333cec4a285bfb9f7794310',1,'erro_chamada_orb_set_interval(void):&#160;erro.h'],['../definicoes__erro_8h.html#a458b66ed01e4550d7230edabace49b51',1,'ERRO_CHAMADA_ORB_SET_INTERVAL():&#160;definicoes_erro.h']]],
  ['erro_5fdescritor_5farquivo',['ERRO_DESCRITOR_ARQUIVO',['../definicoes__erro_8h.html#ab2659b015626919bd57bc786c1fbf074',1,'ERRO_DESCRITOR_ARQUIVO():&#160;definicoes_erro.h'],['../erro_8h.html#a5c3b9c181fd0a39072647f1f14b3ccca',1,'erro_descritor_arquivo(void):&#160;erro.h']]],
  ['erro_5fioctl_5fesc',['ERRO_IOCTL_ESC',['../definicoes__erro_8h.html#a2eca1dc74847482509f915a3b7a76cd4',1,'ERRO_IOCTL_ESC():&#160;definicoes_erro.h'],['../erro_8h.html#a0fc4638a16a86d874ad47c2f9e7e0840',1,'erro_ioctl_esc(void):&#160;erro.h']]],
  ['exemplos',['Exemplos',['../page1.html',1,'']]]
];
