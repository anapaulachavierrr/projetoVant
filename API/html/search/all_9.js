var searchData=
[
  ['sensor_5fcombined_5forb_5fadvertise',['sensor_combined_orb_advertise',['../api_8c.html#a9247ce7c84751e48e98390bf6d53c3e6',1,'sensor_combined_orb_advertise():&#160;api.c'],['../api_8h.html#a83d24a77b5e2e164614baf7909ee37af',1,'sensor_combined_orb_advertise(void):&#160;api.c']]],
  ['sensor_5fcombined_5forb_5fcopy',['sensor_combined_orb_copy',['../api_8c.html#a2e7c7e02fe7ad8121bd275fc02fe94af',1,'sensor_combined_orb_copy():&#160;api.c'],['../api_8h.html#a9c4f268518bbb8ec0b3994cd58d5f82c',1,'sensor_combined_orb_copy(void):&#160;api.c']]],
  ['sensor_5fcombined_5forb_5fset_5finterval',['sensor_combined_orb_set_interval',['../api_8c.html#a7df8892940e47b48c730c75beafec308',1,'sensor_combined_orb_set_interval(int freq):&#160;api.c'],['../api_8h.html#a6aebabefa933ff7eccf9da862f355f1a',1,'sensor_combined_orb_set_interval(int):&#160;api.c']]],
  ['sensor_5fcombined_5forb_5fsubscribe',['sensor_combined_orb_subscribe',['../api_8c.html#ac0146db3e574d699f24312819ae464f8',1,'sensor_combined_orb_subscribe():&#160;api.c'],['../api_8h.html#ac39e23677ca72a58e7e62dd082f5836f',1,'sensor_combined_orb_subscribe(void):&#160;api.c']]],
  ['sensor_5fsub_5ffd',['sensor_sub_fd',['../api_8c.html#a7211b8114f77cf8f9e1f415202c072e4',1,'api.c']]],
  ['status',['status',['../api_8c.html#a6e27f49150e9a14580fb313cc2777e00',1,'api.c']]],
  ['sub_5fgps',['sub_gps',['../api_8c.html#af87625afb51eb7a7652d07eb7c20ae29',1,'api.c']]]
];
