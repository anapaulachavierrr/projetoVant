var searchData=
[
  ['gps_5faltitude',['gps_altitude',['../api_8c.html#a236d47a78f8c0aa290fdc0e244c30890',1,'gps_altitude():&#160;api.c'],['../api_8h.html#a45522dbc77631bc229900bd248342726',1,'gps_altitude(void):&#160;api.c']]],
  ['gps_5flatitude',['gps_latitude',['../api_8c.html#a4f9e29a65d1d26bf5ac7fd4dbb6bfc22',1,'gps_latitude():&#160;api.c'],['../api_8h.html#a3ffec7eff5d7c969d621a60c117bbabc',1,'gps_latitude(void):&#160;api.c']]],
  ['gps_5flongitude',['gps_longitude',['../api_8c.html#ae565c54874147c4af1f98c20ef245120',1,'gps_longitude():&#160;api.c'],['../api_8h.html#a2ba2623d928dc71ee98ef31fde85720a',1,'gps_longitude(void):&#160;api.c']]],
  ['gps_5forb_5fcopy',['gps_orb_copy',['../api_8c.html#a561b832d735663dd39fbd2b294637b4b',1,'gps_orb_copy():&#160;api.c'],['../api_8h.html#a7231b5c1f17597c4d704a629af4b80ea',1,'gps_orb_copy(void):&#160;api.c']]],
  ['gps_5forb_5fpublish',['gps_orb_publish',['../api_8c.html#a78a441bfab785873dfc4999bf433ef4d',1,'gps_orb_publish():&#160;api.c'],['../api_8h.html#ab7363627d10360166d1a96fc2623f645',1,'gps_orb_publish(void):&#160;api.c']]],
  ['gps_5forb_5fsubscribe',['gps_orb_subscribe',['../api_8c.html#a89a1078ea195b037df48642bfdf88387',1,'gps_orb_subscribe():&#160;api.c'],['../api_8h.html#aaa00efff714f27b5be829f07d6ed3d7a',1,'gps_orb_subscribe(void):&#160;api.c']]]
];
