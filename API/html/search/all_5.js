var searchData=
[
  ['intervalo_5fleitura_5fsensor_5fcombined',['INTERVALO_LEITURA_SENSOR_COMBINED',['../definicoes_8h.html#a43a50d5070eb15d907eb418f3bf4ee9b',1,'definicoes.h']]],
  ['ioctl_5fservo_5fdois',['ioctl_servo_dois',['../api_8c.html#a205b416b5985de06d07d26e9b1f69d1d',1,'ioctl_servo_dois(int periodo):&#160;api.c'],['../api_8h.html#ab7a1d0d6487dfb28699f00e2e22ac411',1,'ioctl_servo_dois(int):&#160;api.c'],['../definicoes_8h.html#a707fe23df6f760f7185ef1c2062bbe7b',1,'IOCTL_SERVO_DOIS():&#160;definicoes.h']]],
  ['ioctl_5fservo_5fquatro',['ioctl_servo_quatro',['../api_8c.html#a5c736a238ed94806e99cf5bafaa4bfad',1,'ioctl_servo_quatro(int periodo):&#160;api.c'],['../api_8h.html#ae50838a30f5072ba67a469c54431b63f',1,'ioctl_servo_quatro(int):&#160;api.c'],['../definicoes_8h.html#a2fa5d8e3dbec4c7faa4ae2d4dd972f46',1,'IOCTL_SERVO_QUATRO():&#160;definicoes.h']]],
  ['ioctl_5fservo_5ftres',['ioctl_servo_tres',['../api_8c.html#aabe4510e9574018efd2d839b79dffae2',1,'ioctl_servo_tres(int periodo):&#160;api.c'],['../api_8h.html#a7a82246dc2f774934b2226a1bf46c7eb',1,'ioctl_servo_tres(int):&#160;api.c'],['../definicoes_8h.html#a6178a2903c4bba1f9e86e8810e1f8081',1,'IOCTL_SERVO_TRES():&#160;definicoes.h']]],
  ['ioctl_5fservo_5fum',['IOCTL_SERVO_UM',['../definicoes_8h.html#a39339e9319ab85b0a62990732a67cd48',1,'IOCTL_SERVO_UM():&#160;definicoes.h'],['../api_8c.html#ad8b85483dc4d65790eca9e4fd7e1b3ff',1,'ioctl_servo_um(int periodo):&#160;api.c'],['../api_8h.html#acb41425c3d2c129612377fa5ecfd7825',1,'ioctl_servo_um(int):&#160;api.c']]]
];
